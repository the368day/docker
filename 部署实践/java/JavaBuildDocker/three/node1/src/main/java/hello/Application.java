package hello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

    @Value("${tow-properties}")
    private String version;

    @RequestMapping("/")
    public String home() {
        return "Hello Docker World, version:"+version+",node1";
    }

    public static void main(String[] args) {


        SpringApplication.run(Application.class, args);
    }

}